/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minilibx.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/17 00:02:28 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 22:16:28 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINILIBX_H
# define MINILIBX_H

# include <X11/X.h>
# include <mlx.h>
# include <common.h>

typedef void			*t_mlx;
typedef void			*t_mlx_win;
typedef unsigned int	t_color;

typedef struct	s_image
{
	void		*img;
	char		*addr;
	int			bits_per_pixel;
	int			line_length;
	int			endian;
}				t_image;

typedef struct	s_screen
{
	int			width;
	int			height;
	t_mlx		mlx;
	t_mlx_win	mlx_win;
	t_color		**buffer;
	t_image		displayed;
}				t_screen;

t_color			rgba_to_color(int r, int g, int b, int a);
t_screen		*init_screen(int width, int height, t_bool show_window);
void			image_put_pixel(t_image *image, int x, int y, t_color color);
t_color			image_get_pixel(t_image *image, int x, int y);
void			update_screen(t_screen *screen);
void			delete_screen(t_screen *screen, int max_buf_col);

#endif
