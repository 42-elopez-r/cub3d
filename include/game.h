/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/17 20:32:29 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 22:14:12 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_H
# define GAME_H

# include <minilibx.h>
# include <textures.h>
# include <common.h>
# include <libft.h>

# define ROTATION_ANGLE 0.1
# define ADVANCE_FACTOR 0.5

typedef enum	e_direction
{
	FRONT,
	BACK,
	RIGHT,
	LEFT
}				t_direction;

typedef enum	e_cardinal
{
	NORTH,
	SOUTH,
	EAST,
	WEST
}				t_cardinal;

typedef struct	s_game
{
	t_screen	*screen;

	t_cub_specs	*cub_specs;
	t_textures	*textures;

	int			music_pid;

	double		pos_x;
	double		pos_y;
	double		dir_x;
	double		dir_y;
	double		cam_x;
	double		cam_y;
}				t_game;

typedef struct	s_dda_hit
{
	int			map_x;
	int			map_y;
	t_cardinal	side;
	double		ray_impact;
	double		distance;
}				t_dda_hit;

typedef struct	s_dda_hit_sprite
{
	int			map_x;
	int			map_y;
	double		ray_impact;
	double		distance;
}				t_dda_hit_sprite;

/*
** t_dda_data is intended for internal use of the DDA algorithm only
*/

typedef struct	s_dda_data
{
	double		ray_x;
	double		ray_y;

	double		side_dist_x;
	double		side_dist_y;
	double		delta_dist_x;
	double		delta_dist_y;

	int			step_x;
	int			step_y;

	t_list		*sprite_hits;
}				t_dda_data;

/*
** t_print_sprite_data is intended for internal use of the sprite printing
** algorithm only
*/

typedef struct	s_print_sprite_data
{
	int			stripe_size;
	int			txtr_x;
	double		txtr_y;
	double		y_offset;
	int			stripe_i;
}				t_print_sprite_data;

t_game			*init_game(char *cub_file, t_bool show_window);
void			init_vars_game(t_game *game);
void			delete_game(t_game *game);
int				exit_game(t_game *game);

t_list			*perform_dda(int x, t_game *game, t_dda_hit *dda_hit);
void			raycasting_magic(t_game *game);
int				update_frame(t_game *game);
void			print_stripe(int x, t_game *game, t_dda_hit *dda_hit);
void			print_sprites(int x, t_game *game, t_list *sprite_hits);

int				key_event(int keycode, t_game *game);

void			play_music(t_game *game);
void			play_sound(char *sound);

/*
** Internal use
*/

void			process_sprite(t_game *game, t_dda_hit *dda_hit,
					t_dda_data *dt);
t_color			fade_distance(double distance, t_cub_specs *cub_specs,
					t_color orig);
#endif
