/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/29 16:21:46 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/28 20:45:20 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

typedef enum	e_bool
{
	FALSE = 0,
	TRUE = 1
}				t_bool;

typedef struct	s_rgb
{
	int r;
	int g;
	int b;
}				t_rgb;

typedef struct	s_cub_specs
{
	int			x_resolution;
	int			y_resolution;

	char		*north_texture;
	char		*south_texture;
	char		*west_texture;
	char		*east_texture;
	char		*sprite_texture;

	t_rgb		*floor_color;
	t_rgb		*ceiling_color;

	char		*music;
	char		*steps_sound;
	char		*collision_sound;

	int			map_width;
	int			map_height;
	char		**map;
}				t_cub_specs;

t_cub_specs		*init_cub_specs(void);
void			delete_cub_specs(t_cub_specs *cub_specs);

void			print_error_sys(int error_n, char *message);
void			print_error(char *message);

void			rotation_matrix(double *x, double *y, double rad);
double			length_vector(double x, double y);
double			dot_product(double a_x, double a_y, double b_x, double b_y);

t_bool			ends_with(char *str, char *ending);
#endif
