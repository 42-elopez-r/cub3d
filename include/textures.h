/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/16 23:53:31 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 19:20:32 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURES_H
# define TEXTURES_H

# include <minilibx.h>
# include <common.h>

typedef struct	s_textures
{
	int			width_north;
	int			height_north;
	t_color		**north;

	int			width_south;
	int			height_south;
	t_color		**south;

	int			width_west;
	int			height_west;
	t_color		**west;

	int			width_east;
	int			height_east;
	t_color		**east;

	int			width_sprite;
	int			height_sprite;
	t_color		**sprite;
}				t_textures;

typedef struct	s_texture
{
	int			width;
	int			height;
	t_color		**texture;
}				t_texture;

t_textures		*load_textures(t_cub_specs *cub_specs, t_mlx *mlx);
void			delete_textures(t_textures *textures);

#endif
