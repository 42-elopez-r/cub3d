/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_file_parsing.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/29 16:17:51 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/25 01:05:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB_FILE_PARSING_H
# define CUB_FILE_PARSING_H

# include <common.h>

/*
** External use
*/

t_cub_specs		*parse_specs(char *cub_file);

/*
** Internal use
*/

char			*parse_texture_sound(char *line, t_bool is_texture);
t_bool			parse_resolution(t_cub_specs *cub_specs, char *line);
t_bool			parse_north_texture(t_cub_specs *cub_specs, char *line);
t_bool			parse_south_texture(t_cub_specs *cub_specs, char *line);
t_bool			parse_west_texture(t_cub_specs *cub_specs, char *line);
t_bool			parse_east_texture(t_cub_specs *cub_specs, char *line);
t_bool			parse_sprite_texture(t_cub_specs *cub_specs, char *line);
t_bool			parse_floor_color(t_cub_specs *cub_specs, char *line);
t_bool			parse_ceiling_color(t_cub_specs *cub_specs, char *line);
t_bool			parse_music(t_cub_specs *cub_specs, char *line);
t_bool			parse_steps_sound(t_cub_specs *cub_specs, char *line);
t_bool			parse_collision_sound(t_cub_specs *cub_specs, char *line);
t_bool			parse_map(t_cub_specs *cub_specs, int fd);

#endif
