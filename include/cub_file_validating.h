/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_file_validating.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 17:39:41 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 18:06:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB_FILE_VALIDATING_H
# define CUB_FILE_VALIDATING_H

# include <common.h>

/*
** External use
*/

t_bool	validate_cub_specs(t_cub_specs *cub_specs);

/*
** Internal use
*/

t_bool	is_file_readable(char *path);
t_bool	validate_resolution(t_cub_specs *cub_specs);
t_bool	validate_textures(t_cub_specs *cub_specs);
t_bool	validate_colors(t_cub_specs *cub_specs);
t_bool	validate_sounds(t_cub_specs *cub_specs);
t_bool	validate_map(t_cub_specs *cub_specs);
t_bool	check_map_closure(t_cub_specs *cub_specs);

#endif
