/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_resolution.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/30 13:23:31 by elopez-r          #+#    #+#             */
/*   Updated: 2020/08/02 14:07:56 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_parsing.h>
#include <libft.h>

/*
** Parses the resolution from the line and stores it in cub_specs, returns
** TRUE on success
*/

t_bool	parse_resolution(t_cub_specs *cub_specs, char *line)
{
	static t_bool	already_parsed = FALSE;
	int				i;

	if (already_parsed)
		return (FALSE);
	i = 1;
	while (line[i] == ' ')
		i++;
	if (ft_isdigit(line[i]))
		cub_specs->x_resolution = ft_atoi(line + i);
	else
		return (FALSE);
	while (ft_isdigit(line[i]))
		i++;
	while (line[i] == ' ')
		i++;
	if (ft_isdigit(line[i]))
		cub_specs->y_resolution = ft_atoi(line + i);
	else
		return (FALSE);
	already_parsed = TRUE;
	return (TRUE);
}
