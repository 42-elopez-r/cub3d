/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 15:30:04 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 01:47:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_parsing.h>
#include <get_next_line.h>
#include <libft.h>
#include <stdlib.h>
#include <errno.h>

/*
** Reads the file descriptor (fd) until EOF and stores the non empty lines
** in a t_list, that returns. If no lines are stored or finds empty lines in
** the middle of the map, the t_list will be NULL
*/

static t_list	*get_non_empty_lines(int fd)
{
	char	*line;
	t_list	*lines_list;
	t_bool	map_started;

	map_started = FALSE;
	lines_list = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		if (*line != '\0')
		{
			ft_lstadd_back(&lines_list, ft_lstnew(ft_strdup(line)));
			map_started = TRUE;
		}
		else if (map_started)
		{
			ft_lstclear(&lines_list, free);
			free(line);
			return (NULL);
		}
		free(line);
		line = NULL;
	}
	if (line)
		free(line);
	return (lines_list);
}

/*
** Copy lines contents into cub_specs->map.
*/

static void		copy_list_into_map(t_list *lines, t_cub_specs *cub_specs)
{
	int i;

	i = 0;
	while (lines && i < cub_specs->map_height)
	{
		cub_specs->map[i++] = lines->content;
		lines = lines->next;
	}
}

/*
** Does nothing, intended for freing t_list without erasing its contents
*/

static void		dummy_free(void *x)
{
	(void)x;
}

/*
** Sets cub_specs->map_width to the longest line of the map
*/

static void		set_map_width(t_cub_specs *cub_specs)
{
	int longest;
	int len_current;
	int i;

	longest = 0;
	i = 0;
	while (i < cub_specs->map_height)
	{
		if ((len_current = ft_strlen(cub_specs->map[i])) > longest)
			longest = len_current;
		i++;
	}
	cub_specs->map_width = longest;
}

/*
** Parses the CUB map from the open file descriptor (fd) and stores it
** in cub_specs, returning TRUE on success
*/

t_bool			parse_map(t_cub_specs *cub_specs, int fd)
{
	t_list *lines_list;

	if ((lines_list = get_non_empty_lines(fd)))
	{
		cub_specs->map_height = ft_lstsize(lines_list);
		if (!(cub_specs->map = malloc(sizeof(char*) * cub_specs->map_height)))
		{
			print_error_sys(errno, "Can't store map in memory");
			return (FALSE);
		}
		copy_list_into_map(lines_list, cub_specs);
		ft_lstclear(&lines_list, dummy_free);
		set_map_width(cub_specs);
		return (TRUE);
	}
	else
	{
		print_error("Empty or invalid map");
		return (FALSE);
	}
}
