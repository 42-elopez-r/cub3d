/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_colors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/30 16:01:17 by elopez-r          #+#    #+#             */
/*   Updated: 2020/08/02 14:18:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_parsing.h>
#include <libft.h>
#include <errno.h>

/*
** Parse the RGB colors in the CUB line and store them in color.
** Returns TRUE when success. The line MUST contain at least 2 characters plus
** the null byte
*/

static t_bool	parse_color(t_rgb *color, char *line)
{
	line += 2;
	while (*line == ' ')
		line++;
	if (ft_isdigit(*line))
		color->r = ft_atoi(line);
	else
		return (FALSE);
	while (ft_isdigit(*line))
		line++;
	line++;
	if (ft_isdigit(*line))
		color->g = ft_atoi(line);
	else
		return (FALSE);
	while (ft_isdigit(*line))
		line++;
	line++;
	if (ft_isdigit(*line))
		color->b = ft_atoi(line);
	else
		return (FALSE);
	return (TRUE);
}

/*
** Parse the color of the floor in the CUB line and store them in cub_specs.
** Returns TRUE when success
*/

t_bool			parse_floor_color(t_cub_specs *cub_specs, char *line)
{
	static t_bool already_parsed = FALSE;

	if (already_parsed)
	{
		print_error("Repeated floor color");
		return (FALSE);
	}
	if ((cub_specs->floor_color = malloc(sizeof(t_rgb))))
	{
		already_parsed = TRUE;
		return (parse_color(cub_specs->floor_color, line));
	}
	else
	{
		print_error_sys(errno, "Can't store floor color on memory");
		return (FALSE);
	}
}

/*
** Parse the color of the ceiling in the CUB line and store them in cub_specs.
** Returns TRUE when success
*/

t_bool			parse_ceiling_color(t_cub_specs *cub_specs, char *line)
{
	static t_bool already_parsed = FALSE;

	if (already_parsed)
	{
		print_error("Repeated ceiling color");
		return (FALSE);
	}
	if ((cub_specs->ceiling_color = malloc(sizeof(t_rgb))))
	{
		already_parsed = TRUE;
		return (parse_color(cub_specs->ceiling_color, line));
	}
	else
	{
		print_error_sys(errno, "Can't store ceiling color on memory");
		return (FALSE);
	}
}
