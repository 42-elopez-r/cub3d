/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_common.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 17:55:35 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/25 01:14:43 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_parsing.h>
#include <libft.h>

/*
** Returns the file path referenced in line, storing it in the heap. If
** the path is missing returns NULL. The line MUST contain at least 2
** characters plus null byte. If is_texture is TRUE, it checks that the file
** ends with XPM extension, otherwise checks for WAV extension.
*/

char	*parse_texture_sound(char *line, t_bool is_texture)
{
	if (!ends_with(line, is_texture ? ".xpm" : ".wav"))
	{
		print_error(is_texture ? "Textures must have .xpm extension" :
				"Sounds must have .wav extension");
		return (NULL);
	}
	line += 2;
	while (*line == ' ')
		line++;
	if (*line)
		return (ft_strdup(line));
	else
		return (NULL);
}
