/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_sounds.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 17:59:03 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/25 01:06:57 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_parsing.h>

/*
** Stores the music path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_music(t_cub_specs *cub_specs, char *line)
{
	char			*music;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (music = parse_texture_sound(line, FALSE)))
	{
		already_parsed = TRUE;
		cub_specs->music = music;
		return (TRUE);
	}
	else
		return (FALSE);
}

/*
** Stores the steps sound path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_steps_sound(t_cub_specs *cub_specs, char *line)
{
	char			*sound;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (sound = parse_texture_sound(line, FALSE)))
	{
		already_parsed = TRUE;
		cub_specs->steps_sound = sound;
		return (TRUE);
	}
	else
		return (FALSE);
}

/*
** Stores the collision sound path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_collision_sound(t_cub_specs *cub_specs, char *line)
{
	char			*sound;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (sound = parse_texture_sound(line, FALSE)))
	{
		already_parsed = TRUE;
		cub_specs->collision_sound = sound;
		return (TRUE);
	}
	else
		return (FALSE);
}
