/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_textures.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/30 13:43:25 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/25 01:06:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_parsing.h>

/*
** Stores the north texture path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_north_texture(t_cub_specs *cub_specs, char *line)
{
	char			*texture;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (texture = parse_texture_sound(line, TRUE)))
	{
		already_parsed = TRUE;
		cub_specs->north_texture = texture;
		return (TRUE);
	}
	else
		return (FALSE);
}

/*
** Stores the south texture path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_south_texture(t_cub_specs *cub_specs, char *line)
{
	char			*texture;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (texture = parse_texture_sound(line, TRUE)))
	{
		already_parsed = TRUE;
		cub_specs->south_texture = texture;
		return (TRUE);
	}
	else
		return (FALSE);
}

/*
** Stores the west texture path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_west_texture(t_cub_specs *cub_specs, char *line)
{
	char			*texture;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (texture = parse_texture_sound(line, TRUE)))
	{
		already_parsed = TRUE;
		cub_specs->west_texture = texture;
		return (TRUE);
	}
	else
		return (FALSE);
}

/*
** Stores the east texture path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_east_texture(t_cub_specs *cub_specs, char *line)
{
	char			*texture;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (texture = parse_texture_sound(line, TRUE)))
	{
		already_parsed = TRUE;
		cub_specs->east_texture = texture;
		return (TRUE);
	}
	else
		return (FALSE);
}

/*
** Stores the sprite texture path referenced in line into cub_specs, returning
** TRUE if success
*/

t_bool	parse_sprite_texture(t_cub_specs *cub_specs, char *line)
{
	char			*texture;
	static t_bool	already_parsed = FALSE;

	if (!already_parsed && (texture = parse_texture_sound(line, TRUE)))
	{
		already_parsed = TRUE;
		cub_specs->sprite_texture = texture;
		return (TRUE);
	}
	else
		return (FALSE);
}
