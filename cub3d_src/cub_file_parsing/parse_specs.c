/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_specs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/29 23:28:21 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/25 00:41:19 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_parsing.h>
#include <get_next_line.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

/*
** Returns TRUE if there are unfilled specifications in cub_specs, without
** counting the map
*/

static t_bool	map_specs_unfilled(t_cub_specs *cub_specs)
{
	if (!cub_specs->x_resolution || !cub_specs->y_resolution)
		return (TRUE);
	else if (!cub_specs->north_texture)
		return (TRUE);
	else if (!cub_specs->south_texture)
		return (TRUE);
	else if (!cub_specs->west_texture)
		return (TRUE);
	else if (!cub_specs->east_texture)
		return (TRUE);
	else if (!cub_specs->sprite_texture)
		return (TRUE);
	else if (!cub_specs->floor_color)
		return (TRUE);
	else if (!cub_specs->ceiling_color)
		return (TRUE);
	else if (!cub_specs->music)
		return (TRUE);
	else if (!cub_specs->steps_sound)
		return (TRUE);
	else if (!cub_specs->collision_sound)
		return (TRUE);
	else
		return (FALSE);
}

/*
** Continuation of parse_line function
*/

static t_bool	parse_line_2(t_cub_specs *cub_specs, char *line)
{
	if (line[0] == 'S' && line[1] == 'S')
		return (parse_steps_sound(cub_specs, line));
	else if (line[0] == 'C' && line[1] == 'S')
		return (parse_collision_sound(cub_specs, line));
	else if (line[0] == '\0')
		return (TRUE);
	else
		return (FALSE);
}

/*
** Parses a line from the CUB file storing its info into cub_specs.
** Returns TRUE if success, or FALSE if the line contains an error
*/

static t_bool	parse_line(t_cub_specs *cub_specs, char *line)
{
	if (ft_strlen(line) < 2 && line[0] != '\0')
		return (FALSE);
	else if (line[0] == 'R' && line[1] == ' ')
		return (parse_resolution(cub_specs, line));
	else if (line[0] == 'N' && line[1] == 'O')
		return (parse_north_texture(cub_specs, line));
	else if (line[0] == 'S' && line[1] == 'O')
		return (parse_south_texture(cub_specs, line));
	else if (line[0] == 'W' && line[1] == 'E')
		return (parse_west_texture(cub_specs, line));
	else if (line[0] == 'E' && line[1] == 'A')
		return (parse_east_texture(cub_specs, line));
	else if (line[0] == 'S' && line[1] == ' ')
		return (parse_sprite_texture(cub_specs, line));
	else if (line[0] == 'F' && line[1] == ' ')
		return (parse_floor_color(cub_specs, line));
	else if (line[0] == 'C' && line[1] == ' ')
		return (parse_ceiling_color(cub_specs, line));
	else if (line[0] == 'M' && line[1] == ' ')
		return (parse_music(cub_specs, line));
	else
		return (parse_line_2(cub_specs, line));
}

/*
** Iterates over the open CUB file storing its contents into the t_cub_specs
** It will set *cub_specs to NULL if an error happens
*/

static void		parse_specs_iteration(t_cub_specs **cub_specs, int fd)
{
	char	*line;

	line = NULL;
	while (*cub_specs && map_specs_unfilled(*cub_specs) &&
			get_next_line(fd, &line) == 1)
	{
		if (!parse_line(*cub_specs, line))
		{
			print_error("CUB file with wrong syntax");
			delete_cub_specs(*cub_specs);
			*cub_specs = NULL;
		}
		free(line);
		line = NULL;
	}
	if (line)
		free(line);
	if (*cub_specs &&
			(map_specs_unfilled(*cub_specs) || !parse_map(*cub_specs, fd)))
	{
		delete_cub_specs(*cub_specs);
		*cub_specs = NULL;
	}
}

/*
** Read a CUB file and return a t_cub_specs with its info. Returns NULL if
** an error happens
*/

t_cub_specs		*parse_specs(char *cub_file)
{
	int			fd;
	t_cub_specs	*cub_specs;

	if (!ends_with(cub_file, ".cub"))
	{
		print_error("CUB file must have .cub extension");
		return (NULL);
	}
	if ((fd = open(cub_file, O_RDONLY)) == -1)
	{
		print_error_sys(errno, cub_file);
		return (NULL);
	}
	if ((cub_specs = init_cub_specs()))
		parse_specs_iteration(&cub_specs, fd);
	close(fd);
	return (cub_specs);
}
