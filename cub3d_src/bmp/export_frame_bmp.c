/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export_frame_bmp.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 17:05:14 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/10 21:24:41 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <bmp.h>
#include <common.h>

#define MAGIC_IDENTIFIER 0x4d42
#define FULL_HEADER_SIZE 54
#define DIB_HEADER_SIZE 40

/*
** Returns the bytes for row padding given the width in pixels
*/

static int			get_padding(int pixels_width)
{
	int bytes_width;

	bytes_width = pixels_width * 3;
	if (bytes_width % 4)
		return (4 - (bytes_width % 4));
	else
		return (0);
}

/*
** Initializes the fields of header acording the size of screen
*/

static void			init_header(t_bmp_header *header, t_screen *screen)
{
	header->type = MAGIC_IDENTIFIER;
	header->size = FULL_HEADER_SIZE +
		(screen->width * 3 + get_padding(screen->width)) * screen->height;
	header->reserved1 = 0;
	header->reserved2 = 0;
	header->offset = FULL_HEADER_SIZE;
	header->dib_header_size = DIB_HEADER_SIZE;
	header->width_px = screen->width;
	header->height_px = screen->height;
	header->num_planes = 1;
	header->bits_per_pixel = 24;
	header->compression = 0;
	header->image_size_bytes = header->size - FULL_HEADER_SIZE;
	header->x_resolution_ppm = 0;
	header->y_resolution_ppm = 0;
	header->num_colors = 0;
	header->important_colors = 0;
}

/*
** Stores the RGB conversion of color into rgb
*/

static void			color_to_rgb_24(t_color color, t_rgb_24 *rgb)
{
	rgb->b = color & 255;
	color >>= 8;
	rgb->g = color & 255;
	color >>= 8;
	rgb->r = color & 255;
}

/*
** Assuming the header has already been written, writes the image data from
** screen into the specified file descriptor
*/

static void			write_image_data(int fd, t_screen *screen)
{
	int			x;
	int			y;
	t_rgb_24	rgb;
	char		zero;
	int			i;

	zero = 0;
	y = screen->height;
	while (--y >= 0)
	{
		x = 0;
		while (x < screen->width)
		{
			color_to_rgb_24(screen->buffer[x][y], &rgb);
			write(fd, &rgb, sizeof(t_rgb_24));
			x++;
		}
		i = -1;
		while (++i < get_padding(screen->width))
			write(fd, &zero, 1);
	}
}

/*
** Writes the frame stores in screen into BMP_FILENAME as a BMP image.
*/

void				export_frame_bmp(t_screen *screen)
{
	t_bmp_header	header;
	int				fd;

	init_header(&header, screen);
	if ((fd = open(BMP_FILENAME, O_WRONLY | O_CREAT | O_TRUNC, 0666)) == -1)
	{
		print_error_sys(errno, BMP_FILENAME);
		return ;
	}
	write(fd, &header, sizeof(t_bmp_header));
	write_image_data(fd, screen);
	close(fd);
}
