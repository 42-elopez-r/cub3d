/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffer_manipulation.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/17 17:22:48 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/23 02:33:36 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <minilibx.h>

/*
** Given a RGBA color, return its t_color counterpart
*/

t_color		rgba_to_color(int r, int g, int b, int a)
{
	return (a << 24 | r << 16 | g << 8 | b);
}

/*
** Put a t_color pixel on the (x,y) coordinates of a t_image
*/

void		image_put_pixel(t_image *image, int x, int y, t_color color)
{
	char *dst;

	dst = image->addr + (y * image->line_length +
			x * (image->bits_per_pixel / 8));
	*(t_color *)dst = color;
}

/*
** Get the t_color pixel in the (x,y) coordinates of a t_image
*/

t_color		image_get_pixel(t_image *image, int x, int y)
{
	char *px;

	px = image->addr + (y * image->line_length +
			x * (image->bits_per_pixel / 8));
	return (*(t_color *)px);
}

/*
** Copies screen->buffer content into screen->displayed image
*/

static void	copy_buffer_to_image(t_screen *screen)
{
	int x;
	int y;

	x = 0;
	while (x < screen->width)
	{
		y = 0;
		while (y < screen->height)
		{
			image_put_pixel(&screen->displayed, x, y, screen->buffer[x][y]);
			y++;
		}
		x++;
	}
}

/*
** Displays the current state of screen->buffer in the window
*/

void		update_screen(t_screen *screen)
{
	copy_buffer_to_image(screen);
	mlx_put_image_to_window(screen->mlx, screen->mlx_win,
			screen->displayed.img, 0, 0);
}
