/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/23 01:46:24 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/28 20:44:41 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <math.h>

/*
** Rotate the (x,y) vector rad radians.
*/

void	rotation_matrix(double *x, double *y, double rad)
{
	double new_x;
	double new_y;

	new_x = *x * cos(rad) - *y * sin(rad);
	new_y = *x * sin(rad) + *y * cos(rad);
	*x = new_x;
	*y = new_y;
}

/*
** Returns the length of the (x,y) vector.
*/

double	length_vector(double x, double y)
{
	return (sqrt(x * x + y * y));
}

/*
** Returns the dot product of (a_x,a_y) and (b_x,b_y) vectors.
*/

double	dot_product(double a_x, double a_y, double b_x, double b_y)
{
	return (a_x * b_x + a_y * b_y);
}
