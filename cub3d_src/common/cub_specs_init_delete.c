/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_specs_init_delete.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/29 17:19:21 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 19:51:48 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <stdlib.h>
#include <errno.h>

/*
** Sets all the fields of cub_specs to 0 or NULL
*/

static void	initialize_fields(t_cub_specs *cub_specs)
{
	cub_specs->x_resolution = 0;
	cub_specs->y_resolution = 0;
	cub_specs->north_texture = NULL;
	cub_specs->south_texture = NULL;
	cub_specs->west_texture = NULL;
	cub_specs->east_texture = NULL;
	cub_specs->sprite_texture = NULL;
	cub_specs->floor_color = NULL;
	cub_specs->ceiling_color = NULL;
	cub_specs->music = NULL;
	cub_specs->steps_sound = NULL;
	cub_specs->collision_sound = NULL;
	cub_specs->map_width = 0;
	cub_specs->map_height = 0;
	cub_specs->map = NULL;
}

/*
** Frees the allocated memory of the map field in cub_specs
*/

static void	delete_map(t_cub_specs *cub_specs)
{
	int i;

	i = 0;
	while (i < cub_specs->map_height)
		free(cub_specs->map[i++]);
	free(cub_specs->map);
}

/*
** Allocates in memory a t_cub_specs structure setting every field to zero.
** Returns NULL if can't allocate the memory displaying an error message
*/

t_cub_specs	*init_cub_specs(void)
{
	t_cub_specs	*cub_specs;

	if ((cub_specs = malloc(sizeof(t_cub_specs))))
		initialize_fields(cub_specs);
	else
	{
		print_error_sys(errno, "Can't store CUB specs on memory");
		cub_specs = NULL;
	}
	return (cub_specs);
}

/*
** Frees the allocated memory of a t_cub_specs and its fields
*/

void		delete_cub_specs(t_cub_specs *cub_specs)
{
	if (cub_specs)
	{
		free(cub_specs->north_texture);
		free(cub_specs->south_texture);
		free(cub_specs->west_texture);
		free(cub_specs->east_texture);
		free(cub_specs->sprite_texture);
		free(cub_specs->floor_color);
		free(cub_specs->ceiling_color);
		free(cub_specs->music);
		free(cub_specs->steps_sound);
		free(cub_specs->collision_sound);
		if (cub_specs->map)
			delete_map(cub_specs);
		free(cub_specs);
	}
}
