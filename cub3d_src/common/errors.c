/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/29 23:48:42 by elopez-r          #+#    #+#             */
/*   Updated: 2020/07/30 01:05:47 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <libft.h>
#include <string.h>

/*
** Prints a system error message to stderr with the following structure:
**
** Error:
** [message]: [Description of error_n]
*/

void	print_error_sys(int error_n, char *message)
{
	ft_putendl_fd("Error", 2);
	ft_putstr_fd(message, 2);
	ft_putstr_fd(": ", 2);
	ft_putendl_fd(strerror(error_n), 2);
}

/*
** Prints an error message to stderr with the following structure:
**
** Error:
** [message]
*/

void	print_error(char *message)
{
	ft_putendl_fd("Error", 2);
	ft_putendl_fd(message, 2);
}
