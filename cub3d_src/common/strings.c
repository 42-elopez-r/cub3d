/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strings.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 00:24:27 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/25 00:51:40 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <libft.h>

/*
** Returns TRUE if str ends with ending, FALSE otherwise. CasE iNsEnsItiVe.
*/

t_bool	ends_with(char *str, char *ending)
{
	int i_str;
	int i_ending;

	if (ft_strlen(str) < ft_strlen(ending))
		return (FALSE);
	i_ending = 0;
	i_str = ft_strlen(str) - ft_strlen(ending);
	while (str[i_str])
	{
		if (ft_tolower(str[i_str++]) != ft_tolower(ending[i_ending++]))
			return (FALSE);
	}
	return (TRUE);
}
