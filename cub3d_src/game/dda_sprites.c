/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda_sprites.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 19:12:53 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 00:50:22 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

/*
** The calculations done in the following functions are code implementations
** of the vector and equation systems I made to get the sprites distances
** and ray impacts, so in order to have a chance of understanding what's
** going on you need to read the documentation (TODO: write that documentation)
** and be high on at least 200 µg of LSD.
*/

/*
** Because my equations just doesn't work when game->dir_x is 0 (or very close
** to 0), this function will rotate the direction and camera vectors a tiny bit
** so things doesn't blow. I know is hacky but I've been stuck with this
** problem so much time and this was the least worse solution I could thing of.
*/

static void		move_everything_a_bit(t_game *game)
{
	rotation_matrix(&game->dir_x, &game->dir_y, 0.05);
	rotation_matrix(&game->cam_x, &game->cam_y, 0.05);
}

/*
** Calculates the factor 'h' needed to get the impact vector.
*/

static double	calculate_factor_h(t_game *game, t_dda_data *dt,
		t_dda_hit_sprite *dda_hit_sprite)
{
	double h;
	double f;
	double center_x;
	double center_y;

	if (fabs(game->dir_x) < 0.0001)
		move_everything_a_bit(game);
	center_x = dda_hit_sprite->map_x + 0.5;
	center_y = dda_hit_sprite->map_y + 0.5;
	f = (center_x - game->pos_x -
			((game->dir_x * (center_y - game->pos_y) +
			game->dir_y * (game->pos_x - center_x)) /
			(game->dir_x * game->cam_y - game->dir_y * game->cam_x)) *
			game->cam_x) / game->dir_x;
	h = (f * game->dir_x + (f *
			(dt->ray_y * game->dir_x - dt->ray_x * game->dir_y) /
			(dt->ray_x * game->cam_y - dt->ray_y * game->cam_x)) *
			game->cam_x) / (dt->ray_x == 0 ? 0.0000001 : dt->ray_x);
	return (h);
}

/*
** Calculates the ray impact in the sprite and stores it in
** dda_hit_sprite->ray_impact. Returns TRUE if it's in the interval [0, 1).
*/

static t_bool	calculate_ray_impact(t_game *game, t_dda_data *dt,
		t_dda_hit_sprite *dda_hit_sprite)
{
	double h;
	double impact_x;
	double impact_y;
	double lenght_center_impact;
	double dot_product_cam_centerimpact;

	h = calculate_factor_h(game, dt, dda_hit_sprite);
	impact_x = game->pos_x + h * dt->ray_x;
	impact_y = game->pos_y + h * dt->ray_y;
	lenght_center_impact = length_vector(
			impact_x - (dda_hit_sprite->map_x + 0.5),
			impact_y - (dda_hit_sprite->map_y + 0.5));
	dot_product_cam_centerimpact = dot_product(game->cam_x, game->cam_y,
			impact_x - (dda_hit_sprite->map_x + 0.5),
			impact_y - (dda_hit_sprite->map_y + 0.5));
	if (fabs(dot_product_cam_centerimpact - length_vector(game->cam_x,
			game->cam_y) * lenght_center_impact) < 0.001)
		dda_hit_sprite->ray_impact = 0.5 + lenght_center_impact;
	else
		dda_hit_sprite->ray_impact = 0.5 - lenght_center_impact;
	return (dda_hit_sprite->ray_impact >= 0 && dda_hit_sprite->ray_impact < 1);
}

/*
** If the current cell analized by DDA is a sprite, adds a t_dda_hit_sprite to
** dt->sprite_hits with the pertinent data.
*/

void			process_sprite(t_game *game, t_dda_hit *dda_hit, t_dda_data *dt)
{
	t_dda_hit_sprite *dda_hit_sprite;

	if (game->cub_specs->map[dda_hit->map_y][dda_hit->map_x] == '2')
	{
		if (!(dda_hit_sprite = malloc(sizeof(t_dda_hit_sprite))))
		{
			print_error_sys(errno, "Can't allocate DDA data for sprite");
			return ;
		}
		dda_hit_sprite->map_x = dda_hit->map_x;
		dda_hit_sprite->map_y = dda_hit->map_y;
		dda_hit_sprite->distance = length_vector(dda_hit->map_x + 0.5 -
				game->pos_x, dda_hit->map_y + 0.5 - game->pos_y);
		if (calculate_ray_impact(game, dt, dda_hit_sprite))
			ft_lstadd_front(&dt->sprite_hits, ft_lstnew(dda_hit_sprite));
		else
			free(dda_hit_sprite);
	}
}
