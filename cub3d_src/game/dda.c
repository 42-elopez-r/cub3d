/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 00:17:23 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 13:09:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <math.h>

/*
** Initializes the first DDA data:
**
**  - dda_hit->map_x/y => Current coordinates in the map
**  - dt->ray_x/y => Ray vector
**  - dt->delta_dist_x/y => proportional lenght between x/y sides over the ray
*/

static void		init_dda_1(int x, t_game *game, t_dda_hit *dda_hit,
		t_dda_data *dt)
{
	double cam_ray_factor;

	dda_hit->map_x = game->pos_x;
	dda_hit->map_y = game->pos_y;
	cam_ray_factor = 2 * x / (double)game->screen->width - 1;
	dt->ray_x = game->dir_x + game->cam_x * cam_ray_factor;
	dt->ray_y = game->dir_y + game->cam_y * cam_ray_factor;
	dt->delta_dist_x = fabs(1 / dt->ray_x);
	dt->delta_dist_y = fabs(1 / dt->ray_y);
}

/*
** Initializes the rest of DDA data:
**  - dt->step_x/y => 1 or -1 indicates the direction in which the ray advances
**  - dt_side_dist_x/y => lenght to the first x/y sides over the ray
*/

static void		init_dda_2(t_game *game, t_dda_hit *dda_hit, t_dda_data *dt)
{
	if (dt->ray_x < 0)
	{
		dt->step_x = -1;
		dt->side_dist_x = (game->pos_x - dda_hit->map_x) * dt->delta_dist_x;
	}
	else
	{
		dt->step_x = 1;
		dt->side_dist_x = (1 - game->pos_x + dda_hit->map_x) * dt->delta_dist_x;
	}
	if (dt->ray_y < 0)
	{
		dt->step_y = -1;
		dt->side_dist_y = (game->pos_y - dda_hit->map_y) * dt->delta_dist_y;
	}
	else
	{
		dt->step_y = 1;
		dt->side_dist_y = (1 - game->pos_y + dda_hit->map_y) * dt->delta_dist_y;
	}
}

/*
** Runs the DDA algorithm storing the hit wall in dda_hit->map_x/y and
** returning TRUE if it was a y_side hit and FALSE if it hit a x_side instead
*/

static t_bool	run_dda(t_game *game, t_dda_hit *dda_hit, t_dda_data *dt)
{
	t_bool hit;
	t_bool side_y;

	dt->sprite_hits = NULL;
	hit = FALSE;
	while (!hit)
	{
		if (dt->side_dist_x < dt->side_dist_y)
		{
			dt->side_dist_x += dt->delta_dist_x;
			dda_hit->map_x += dt->step_x;
			side_y = FALSE;
		}
		else
		{
			dt->side_dist_y += dt->delta_dist_y;
			dda_hit->map_y += dt->step_y;
			side_y = TRUE;
		}
		if (game->cub_specs->map[dda_hit->map_y][dda_hit->map_x] == '1')
			hit = TRUE;
		else
			process_sprite(game, dda_hit, dt);
	}
	return (side_y);
}

/*
** Calculates the exact part of the side of the wall where the ray hit
** storing it in dda_hit->ray_impact.
** For this calculation, the postion and ray vectors, the wall coords and
** the side of the impact are needed.
*/

static void		calculate_ray_impact(t_game *game, t_dda_data *dt,
		t_dda_hit *dda_hit)
{
	double factor;

	if (dda_hit->side == EAST || dda_hit->side == WEST)
	{
		factor = (dda_hit->map_x + (dda_hit->side == EAST ? 1 : 0) -
				game->pos_x) / dt->ray_x;
		dda_hit->ray_impact = game->pos_y + factor * dt->ray_y;
	}
	else
	{
		factor = (dda_hit->map_y + (dda_hit->side == SOUTH ? 1 : 0) -
				game->pos_y) / dt->ray_y;
		dda_hit->ray_impact = game->pos_x + factor * dt->ray_x;
	}
	dda_hit->ray_impact = dda_hit->ray_impact - (int)dda_hit->ray_impact;
	if (dda_hit->side == NORTH || dda_hit->side == EAST)
		dda_hit->ray_impact = 1 - dda_hit->ray_impact;
}

/*
** For a given X coordinate, calculates the coords of the wall hit by a ray,
** the exact part of the wall side that was hit, if the side hit was N/S/E/W
** and the distance perpendicular to the camera vector to the wall (instead
** of calculating distance from the position vector to avoid fisheye effect)
** storing that data into dda_hit. The function will also return a list with
** the coords, distance and ray impact of the sprites found on the way, or
** NULL if none was found.
*/

t_list			*perform_dda(int x, t_game *game, t_dda_hit *dda_hit)
{
	t_dda_data	dt;
	t_bool		side_y;

	init_dda_1(x, game, dda_hit, &dt);
	init_dda_2(game, dda_hit, &dt);
	side_y = run_dda(game, dda_hit, &dt);
	if (side_y)
		dda_hit->side = game->pos_y > dda_hit->map_y ? SOUTH : NORTH;
	else
		dda_hit->side = game->pos_x > dda_hit->map_x ? EAST : WEST;
	calculate_ray_impact(game, &dt, dda_hit);
	if (side_y)
	{
		dda_hit->distance = ((dda_hit->map_y - game->pos_y +
				(1 - dt.step_y) / 2) / dt.ray_y);
	}
	else
	{
		dda_hit->distance = ((dda_hit->map_x - game->pos_x +
				(1 - dt.step_x) / 2) / dt.ray_x);
	}
	return (dt.sprite_hits);
}
