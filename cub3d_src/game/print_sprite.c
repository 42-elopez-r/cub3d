/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_sprite.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/29 00:06:23 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/11 21:00:12 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>

/*
** Initializes the variables needed for the printing stripe algorithm inside
** the dt structure
*/

static void	init_print_sprite_data(t_print_sprite_data *dt, t_game *game,
		t_dda_hit_sprite *dda_sprite)
{
	dt->stripe_size = game->screen->height / dda_sprite->distance;
	dt->txtr_x = (int)(game->textures->width_sprite * dda_sprite->ray_impact);
	dt->y_offset = (double)game->textures->height_sprite / dt->stripe_size;
	dt->stripe_i = (game->screen->height / 2) - (dt->stripe_size / 2);
	dt->txtr_y = 0;
	if (dt->stripe_i < 0)
	{
		dt->txtr_y = -1 * dt->stripe_i * dt->y_offset;
		dt->stripe_i = 0;
	}
}

/*
** Prints the stripe of a sprite in the x column of the window buffer.
*/

static void	print_sprite(int x, t_game *game, t_dda_hit_sprite *dda_sprite)
{
	t_print_sprite_data dt;

	init_print_sprite_data(&dt, game, dda_sprite);
	while (dt.txtr_y < game->textures->height_sprite &&
			dt.stripe_i < game->screen->height)
	{
		if (game->textures->sprite[dt.txtr_x][(int)dt.txtr_y] != 0xFF000000)
		{
			game->screen->buffer[x][dt.stripe_i] = fade_distance(
					dda_sprite->distance, game->cub_specs,
					game->textures->sprite[dt.txtr_x][(int)dt.txtr_y]);
		}
		dt.stripe_i++;
		dt.txtr_y += dt.y_offset;
	}
}

/*
** Prints all the sprites for the x column of the window buffer.
*/

void		print_sprites(int x, t_game *game, t_list *sprite_hits)
{
	while (sprite_hits)
	{
		print_sprite(x, game, sprite_hits->content);
		sprite_hits = sprite_hits->next;
	}
}
