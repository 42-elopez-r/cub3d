/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stripe.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 17:24:20 by elopez-r          #+#    #+#             */
/*   Updated: 2020/11/19 01:08:59 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>

/*
** Draws in the x stripe of the window buffer half the color of the floor
** and half the color of the ceiling.
*/

static void		print_floor_ceiling(int x, t_game *game)
{
	int		i;
	t_rgb	*cl;
	t_rgb	*fl;

	cl = game->cub_specs->ceiling_color;
	fl = game->cub_specs->floor_color;
	i = 0;
	while (i < game->screen->height / 2)
		game->screen->buffer[x][i++] = rgba_to_color(cl->r, cl->g, cl->b, 0);
	while (i < game->screen->height)
		game->screen->buffer[x][i++] = rgba_to_color(fl->r, fl->g, fl->b, 0);
}

/*
** Asigns the right texture to "texture" fields from "textures" according to
** the desired side.
*/

static void		get_texture(t_textures *textures, t_cardinal side,
		t_texture *texture)
{
	if (side == NORTH)
	{
		texture->width = textures->width_north;
		texture->height = textures->height_north;
		texture->texture = textures->north;
	}
	else if (side == SOUTH)
	{
		texture->width = textures->width_south;
		texture->height = textures->height_south;
		texture->texture = textures->south;
	}
	else if (side == WEST)
	{
		texture->width = textures->width_west;
		texture->height = textures->height_west;
		texture->texture = textures->west;
	}
	else
	{
		texture->width = textures->width_east;
		texture->height = textures->height_east;
		texture->texture = textures->east;
	}
}

/*
** Fades to black a given color given a factor that should be between 0 and 1.
** If factor is not between that interval, it returns the unaltered color.
*/

static t_color	fade_color(t_color orig, double factor)
{
	int a;
	int r;
	int g;
	int b;

	if (factor < 0 || factor > 1)
		return (orig);
	b = (int)((orig & 255) * factor);
	g = (int)(((orig >> 8) & 255) * factor);
	r = (int)(((orig >> 16) & 255) * factor);
	a = (int)(((orig >> 24) & 255) * factor);
	return (rgba_to_color(r, g, b, a));
}

/*
** Returns a faded color based on the distance it was found. If the distance
** is smaller than one third of the biggest side of the map it doesn't apply
** the fading.
*/

t_color			fade_distance(double distance, t_cub_specs *cub_specs,
		t_color orig)
{
	int max_distance;

	max_distance = cub_specs->map_width > cub_specs->map_height ?
		cub_specs->map_width : cub_specs->map_height;
	if (distance > max_distance * 0.33)
		return (fade_color(orig, 1 - distance / max_distance + 0.3));
	else
		return (orig);
}

/*
** Prints into the x column of the window buffer the right texture according
** to the DDA analysis results, plus the floor and ceiling colors.
*/

void			print_stripe(int x, t_game *game, t_dda_hit *dda_hit)
{
	t_texture	txtr;
	int			txtr_x;
	double		txtr_y;
	double		y_offset;
	int			stripe_i;

	print_floor_ceiling(x, game);
	get_texture(game->textures, dda_hit->side, &txtr);
	txtr_x = (int)((txtr.width - 1) * dda_hit->ray_impact);
	y_offset = ((double)txtr.height) /
		(game->screen->height / dda_hit->distance);
	stripe_i = (game->screen->height / 2) -
		((game->screen->height / dda_hit->distance) / 2);
	txtr_y = 0;
	if (stripe_i < 0)
	{
		txtr_y = -1 * stripe_i * y_offset;
		stripe_i = 0;
	}
	while (txtr_y < txtr.height && stripe_i < game->screen->height)
	{
		game->screen->buffer[x][stripe_i++] = fade_distance(dda_hit->distance,
				game->cub_specs, txtr.texture[txtr_x][(int)txtr_y]);
		txtr_y += y_offset;
	}
}
