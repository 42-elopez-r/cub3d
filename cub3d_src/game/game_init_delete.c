/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_init_delete.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/17 20:37:04 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 22:29:24 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <cub_file_parsing.h>
#include <cub_file_validating.h>
#include <common.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>

/*
** Loads the specified cub file into game->cub_file. If the map is invalid
** or it can't be parsed, game will be deleted. Returns TRUE on success, FALSE
** otherwise.
*/

static t_bool	load_cub_specs(t_game *game, char *cub_file)
{
	if ((game->cub_specs = parse_specs(cub_file)))
	{
		if (!validate_cub_specs(game->cub_specs))
		{
			print_error("Invalid cub file");
			delete_game(game);
			return (FALSE);
		}
		return (TRUE);
	}
	else
	{
		delete_game(game);
		return (FALSE);
	}
}

/*
** Sets the direction and camera vectors according to the desired orientation
** (N, S, W, E)
*/

static void		set_orientation(t_game *game, char orient)
{
	double	rad;

	game->dir_x = -1;
	game->dir_y = 0;
	game->cam_x = 0;
	game->cam_y = -0.66;
	if (orient == 'W')
		return ;
	else if (orient == 'N')
		rad = M_PI_2;
	else if (orient == 'E')
		rad = M_PI;
	else
		rad = M_PI + M_PI_2;
	rotation_matrix(&game->dir_x, &game->dir_y, rad);
	rotation_matrix(&game->cam_x, &game->cam_y, rad);
}

/*
** Sets the position, direction and camera vectors acording to the initial
** cell set in game->cub_specs->map
*/

static void		init_vectors(t_game *game)
{
	int		x;
	int		y;

	x = 0;
	while (x < game->cub_specs->map_width)
	{
		y = 0;
		while (y < game->cub_specs->map_height)
		{
			if (game->cub_specs->map[y][x] == 'N' ||
					game->cub_specs->map[y][x] == 'S' ||
					game->cub_specs->map[y][x] == 'E' ||
					game->cub_specs->map[y][x] == 'W')
			{
				game->pos_x = x + 0.5;
				game->pos_y = y + 0.5;
				set_orientation(game, game->cub_specs->map[y][x]);
				return ;
			}
			y++;
		}
		x++;
	}
}

/*
** Initializes a t_game given a .cub file. If show_window is false, it won't
** create a window and it won't start playing the music.
** Returns NULL when an error happens
*/

t_game			*init_game(char *cub_file, t_bool show_window)
{
	t_game *game;

	if ((game = malloc(sizeof(t_game))))
	{
		init_vars_game(game);
		if (!load_cub_specs(game, cub_file))
			return (NULL);
		if (!(game->screen = init_screen(game->cub_specs->x_resolution,
				game->cub_specs->y_resolution, show_window)))
		{
			delete_game(game);
			return (NULL);
		}
		if (!(game->textures = load_textures(game->cub_specs,
				game->screen->mlx)))
		{
			delete_game(game);
			return (NULL);
		}
		init_vectors(game);
		if (show_window)
			play_music(game);
		return (game);
	}
	return (NULL);
}

/*
** Frees the memory of a t_game and its contents
*/

void			delete_game(t_game *game)
{
	if (game)
	{
		if (game->music_pid != -1)
			kill(game->music_pid, SIGTERM);
		if (game->screen)
			delete_screen(game->screen, 0);
		if (game->cub_specs)
			delete_cub_specs(game->cub_specs);
		if (game->textures)
			delete_textures(game->textures);
		free(game);
	}
}
