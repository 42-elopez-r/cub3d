/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controls.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/23 01:44:47 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/14 00:39:23 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <math.h>

/*
** Rotates the direction vector.
*/

static void		rotate(t_game *game, int clockwise)
{
	double angle;

	angle = ROTATION_ANGLE * (clockwise ? 1.0 : -1.0);
	rotation_matrix(&game->dir_x, &game->dir_y, angle);
	rotation_matrix(&game->cam_x, &game->cam_y, angle);
}

/*
** Continuation of the calculate_new_position function below to comply with
** the norm. Takes care of the LEFT and RIGHT direction cases.
*/

static void		calculate_new_position2(t_game *game, t_direction direction,
		double *new_x, double *new_y)
{
	double rot_dir_x;
	double rot_dir_y;

	rot_dir_x = game->dir_x;
	rot_dir_y = game->dir_y;
	rotation_matrix(&rot_dir_x, &rot_dir_y, M_PI_2);
	if (direction == LEFT)
	{
		*new_x = game->pos_x - rot_dir_x * ADVANCE_FACTOR;
		*new_y = game->pos_y - rot_dir_y * ADVANCE_FACTOR;
	}
	else
	{
		*new_x = game->pos_x + rot_dir_x * ADVANCE_FACTOR;
		*new_y = game->pos_y + rot_dir_y * ADVANCE_FACTOR;
	}
}

/*
** Calculates the new coordinates of the position advancing in the given
** direction.
*/

static void		calculate_new_position(t_game *game, t_direction direction,
		double *new_x, double *new_y)
{
	if (direction == FRONT)
	{
		*new_x = game->pos_x + game->dir_x * ADVANCE_FACTOR;
		*new_y = game->pos_y + game->dir_y * ADVANCE_FACTOR;
	}
	else if (direction == BACK)
	{
		*new_x = game->pos_x - game->dir_x * ADVANCE_FACTOR;
		*new_y = game->pos_y - game->dir_y * ADVANCE_FACTOR;
	}
	else
		calculate_new_position2(game, direction, new_x, new_y);
}

/*
** Advances the position in the specified direction if there's no collision.
*/

static void		advance(t_game *game, t_direction direction)
{
	double new_x;
	double new_y;

	calculate_new_position(game, direction, &new_x, &new_y);
	if (game->cub_specs->map[(int)new_y][(int)new_x] != '1' &&
			game->cub_specs->map[(int)new_y][(int)new_x] != '2')
	{
		game->pos_x = new_x;
		game->pos_y = new_y;
		play_sound(game->cub_specs->steps_sound);
	}
	else
		play_sound(game->cub_specs->collision_sound);
}

/*
** This function receives a keycode from the Minilibx hook and redirects to
** the pertinent action. Then, it updates the frame.
*/

int				key_event(int keycode, t_game *game)
{
	if (keycode == 'w')
		advance(game, FRONT);
	else if (keycode == 'a')
		advance(game, LEFT);
	else if (keycode == 's')
		advance(game, BACK);
	else if (keycode == 'd')
		advance(game, RIGHT);
	else if (keycode == 65361)
		rotate(game, 0);
	else if (keycode == 65363)
		rotate(game, 1);
	else if (keycode == 65307)
		exit_game(game);
	update_frame(game);
	return (0);
}
