/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_init_delete_2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 19:11:25 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 20:08:04 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <stddef.h>

/*
** Initializes some variables of a t_game to their default values
*/

void	init_vars_game(t_game *game)
{
	game->cub_specs = NULL;
	game->textures = NULL;
	game->screen = NULL;
	game->music_pid = -1;
}
