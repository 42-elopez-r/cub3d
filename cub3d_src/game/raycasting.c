/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 16:30:04 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 22:13:54 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <stdlib.h>

/*
** This function draws a frame in game->screen->buffer according to the
** current state of the game using raycasting magic.
*/

void		raycasting_magic(t_game *game)
{
	int			x;
	t_dda_hit	dda_hit;
	t_list		*sprite_hits;

	x = 0;
	while (x < game->screen->width)
	{
		sprite_hits = perform_dda(x, game, &dda_hit);
		print_stripe(x, game, &dda_hit);
		if (sprite_hits)
		{
			print_sprites(x, game, sprite_hits);
			ft_lstclear(&sprite_hits, free);
		}
		x++;
	}
}

/*
** This function updates the frame displayed in the window according to the
** current state of the game. Always returns 0 (this is so it can be called
** from mlx_hook events)
*/

int			update_frame(t_game *game)
{
	raycasting_magic(game);
	update_screen(game->screen);
	return (0);
}
