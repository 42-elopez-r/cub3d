/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   music.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 18:23:43 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/12 20:46:45 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <stdlib.h>

/*
** So in this file some extra functions are used to make the music bonus.
** This functions are:
**  - fork: It allows to spawn another process. It is used to have a process
**          spawning the background music in loop, and another one with the
**          music player in parallel to the raycaster execution.
**  - execlp: Replaces the calling process with the aplay music player,
**            allowing to keep track of its PID, retrieved with fork.
**  - waitpid: In the music launcher process, it waits for the song to finish
**             so it can then play it again.
**  - kill: Once the program must be closed, allows to kill the launcher
**          process and the running aplay.
**  - signal: Allows the music launcher process to set a function to be
**            executed when receiving a SIGTERM, so before ending it can
**            kill the running aplay.
**
** Here I also make use of a global variable (g_pid_aplay). This is so the
** signal handler can know the PID of aplay, because signal handlers can't
** receive any parameters and global variables are the only way to pass them
** information.
*/

int			g_pid_aplay;

/*
** Signal handler of music launcher process. When receiving a SIGTERM, this
** function will kill aplay and then exit the process.
*/

static void	aplay_killer(int sig)
{
	(void)sig;
	if (g_pid_aplay != -1)
		kill(g_pid_aplay, SIGTERM);
	exit(0);
}

/*
** Spawns aplay playing in loop the specified music file. To stop the loop and
** aplay, send a SIGTERM to the process that called this function.
*/

static void	launcher_process(char *music)
{
	int status;

	signal(SIGTERM, aplay_killer);
	while (1)
	{
		if ((g_pid_aplay = fork()) == -1)
		{
			print_error_sys(errno, "Can't spawn aplay process");
			g_pid_aplay = -1;
			exit(1);
		}
		else if (g_pid_aplay == 0)
		{
			if (execlp("paplay", "paplay", music, NULL) == -1)
			{
				if (execlp("aplay", "aplay", "-q", music, NULL) == -1)
					exit(1);
			}
		}
		else
			waitpid(g_pid_aplay, &status, 0);
	}
}

/*
** Spawns the music launcher process storing its PID into game->music_pid.
** If the process can't be spawn, sets game->music_pid to -1
*/

void		play_music(t_game *game)
{
	int pid;

	if ((pid = fork()) == -1)
	{
		print_error_sys(errno, "Can't spawn music launcher process");
		game->music_pid = -1;
	}
	else if (pid == 0)
		launcher_process(game->cub_specs->music);
	else
		game->music_pid = pid;
}

/*
** Plays sound in a new process.
*/

void		play_sound(char *sound)
{
	int pid;

	if ((pid = fork()) == -1)
		print_error_sys(errno, "Can't aplay process");
	else if (pid == 0)
	{
		if (execlp("paplay", "paplay", sound, NULL) == -1)
			execlp("aplay", "aplay", "-q", sound, NULL);
		exit(0);
	}
}
