/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures_init_delete.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/17 00:11:40 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/23 02:55:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <textures.h>
#include <libft.h>
#include <stdlib.h>

/*
** Allocates in the heap a t_color based image of the especified size,
** returning NULL if an error happened
*/

static t_color	**allocate_image(int width, int height)
{
	t_color	**image;
	int		i;

	if ((image = ft_calloc(width, sizeof(t_color*))))
	{
		i = 0;
		while (i < width)
		{
			if (!(image[i] = ft_calloc(height, sizeof(t_color))))
			{
				print_error("Failed memory reservation for a texture");
				while (i)
					free(image[i--]);
				free(image);
				return (NULL);
			}
			i++;
		}
		return (image);
	}
	print_error("Failed memory reservation for a texture");
	return (NULL);
}

/*
** Reads a texture from a file returning its content and storing its
** resolution in the width and height parameters. Returns NULL if error
** happened.
*/

static t_color	**load_texture(t_mlx *mlx, char *filename, int *width,
		int *height)
{
	t_image image;
	t_color **image_color;
	int		y;
	int		x;

	if ((image.img = mlx_xpm_file_to_image(mlx, filename, width, height)))
	{
		image.addr = mlx_get_data_addr(image.img, &image.bits_per_pixel,
				&image.line_length, &image.endian);
		if (!(image_color = allocate_image(*width, *height)))
		{
			mlx_destroy_image(mlx, image.img);
			return (NULL);
		}
		x = -1;
		while (++x < *width)
		{
			y = -1;
			while (++y < *height)
				image_color[x][y] = image_get_pixel(&image, x, y);
		}
		mlx_destroy_image(mlx, image.img);
		return (image_color);
	}
	return (NULL);
}

/*
** Frees the memory of a given t_color based texture
*/

static void		delete_texture(t_color **texture, int width)
{
	if (texture)
	{
		while (width--)
			free(texture[width]);
		free(texture);
	}
}

/*
** Frees the memory of a t_textures and its contents
*/

void			delete_textures(t_textures *textures)
{
	if (textures)
	{
		delete_texture(textures->north, textures->width_north);
		delete_texture(textures->south, textures->width_south);
		delete_texture(textures->west, textures->width_west);
		delete_texture(textures->east, textures->width_east);
		delete_texture(textures->sprite, textures->width_sprite);
		free(textures);
	}
}

/*
** Returns a t_textures loaded with the textures specified in cub_specs.
** Returns NULL if error happened. Don't forget passing mlx, needed for
** reading the XPM textures.
*/

t_textures		*load_textures(t_cub_specs *cub_specs, t_mlx *mlx)
{
	t_textures *textures;

	if ((textures = malloc(sizeof(t_textures))))
	{
		textures->north = load_texture(mlx, cub_specs->north_texture,
				&textures->width_north, &textures->height_north);
		textures->south = load_texture(mlx, cub_specs->south_texture,
				&textures->width_south, &textures->height_south);
		textures->west = load_texture(mlx, cub_specs->west_texture,
				&textures->width_west, &textures->height_west);
		textures->east = load_texture(mlx, cub_specs->east_texture,
				&textures->width_east, &textures->height_east);
		textures->sprite = load_texture(mlx, cub_specs->sprite_texture,
				&textures->width_sprite, &textures->height_sprite);
		if (textures->north && textures->south && textures->west &&
				textures->east && textures->sprite)
			return (textures);
		else
		{
			print_error("Error when reading one or more texture files");
			delete_textures(textures);
			return (NULL);
		}
	}
	return (NULL);
}
