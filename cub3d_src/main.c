/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/23 02:30:28 by elopez-r          #+#    #+#             */
/*   Updated: 2020/12/13 22:19:42 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <game.h>
#include <libft.h>
#include <common.h>
#include <bmp.h>

/*
** This function receives the argc and argv, storing the .cub in *cub_file and
** storing TRUE in *save_bmp if the --save option was used. Returns FALSE if the
** user passed wrong arguments.
*/

static t_bool	parse_arguments(int argc, char *argv[], char **cub_file,
		t_bool *save_bmp)
{
	if (argc == 2)
	{
		*cub_file = argv[1];
		*save_bmp = FALSE;
		return (TRUE);
	}
	else if (argc == 3 &&
		ft_strncmp("--save", argv[2], ft_strlen(argv[1])) == 0)
	{
		*cub_file = argv[1];
		*save_bmp = TRUE;
		return (TRUE);
	}
	else
	{
		print_error("Wrong arguments");
		return (FALSE);
	}
}

/*
** For some reason, the program doesn't work if I delete this function /s.
*/

int				main(int argc, char *argv[])
{
	t_game	*game;
	char	*cub_file;
	t_bool	save_bmp;

	if (parse_arguments(argc, argv, &cub_file, &save_bmp))
	{
		if (!(game = init_game(cub_file, !save_bmp)))
			return (1);
		if (save_bmp)
		{
			raycasting_magic(game);
			export_frame_bmp(game->screen);
			exit_game(game);
		}
		update_frame(game);
		mlx_key_hook(game->screen->mlx_win, key_event, game);
		mlx_hook(game->screen->mlx_win, ClientMessage, StructureNotifyMask,
			exit_game, game);
		mlx_hook(game->screen->mlx_win, VisibilityNotify, VisibilityChangeMask,
			update_frame, game);
		mlx_loop(game->screen->mlx);
		return (0);
	}
	else
		return (1);
}
