/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_closure.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 16:59:11 by elopez-r          #+#    #+#             */
/*   Updated: 2020/08/02 18:53:28 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>
#include <libft.h>

/*
** Returns TRUE if the cell c is must be inside walls (0, 2, N, S, W, E)
*/

static t_bool	must_be_inside(char c)
{
	return (c == '0' || c == '2' || c == 'N' || c == 'S' || c == 'W' ||
			c == 'E');
}

/*
** Returns TRUE if the cell map[i][j] is correctly surrounded. A cell that
** must be inside walls will always have to be surrounded on its four sides
** by another one of that kind or a wall
*/

static t_bool	check_cell(int i, int j, int map_height, char **map)
{
	int top_line_len;
	int bottom_line_len;

	if (!must_be_inside(map[i][j]))
		return (TRUE);
	else if (i == 0 || j == 0 || map[i][j + 1] == '\0' || i == map_height - 1)
		return (FALSE);
	top_line_len = ft_strlen(map[i - 1]);
	bottom_line_len = ft_strlen(map[i + 1]);
	if (j >= top_line_len || j >= bottom_line_len || map[i][j - 1] == ' ' ||
			map[i][j + 1] == ' ' || map[i - 1][j] == ' ' ||
			map[i + 1][j] == ' ')
		return (FALSE);
	else
		return (TRUE);
}

/*
** Returns TRUE if the map is contained by walls
*/

t_bool			check_map_closure(t_cub_specs *cub_specs)
{
	int i;
	int j;
	int line_len;

	i = 0;
	while (i < cub_specs->map_height)
	{
		line_len = ft_strlen(cub_specs->map[i]);
		j = 0;
		while (j < line_len)
		{
			if (!check_cell(i, j, cub_specs->map_height, cub_specs->map))
			{
				print_error("Map walls not closed");
				return (FALSE);
			}
			j++;
		}
		i++;
	}
	return (TRUE);
}
