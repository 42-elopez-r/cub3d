/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_common.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 18:07:36 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 18:08:17 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

/*
** Return TRUE if the file referenced by path is readable. Displays an error
** message if it isn't
*/

t_bool	is_file_readable(char *path)
{
	int fd;

	if ((fd = open(path, O_RDONLY)) > -1)
	{
		close(fd);
		return (TRUE);
	}
	else
	{
		print_error_sys(errno, path);
		return (FALSE);
	}
}
