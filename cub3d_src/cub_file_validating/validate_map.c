/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_map.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 20:43:11 by elopez-r          #+#    #+#             */
/*   Updated: 2020/08/02 18:34:34 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>
#include <libft.h>

/*
** Returns TRUE if c is a valid character. Also checks that the star
** position characters (N,S,E,W) appear only once
*/

static t_bool	validate_character(t_bool *start_position, char c)
{
	if (c == 'N' || c == 'S' || c == 'E' || c == 'W')
	{
		if (*start_position)
			return (FALSE);
		else
		{
			*start_position = TRUE;
			return (TRUE);
		}
	}
	else if ((c >= '0' && c <= '2') || c == ' ')
		return (TRUE);
	else
		return (FALSE);
}

/*
** Returns TRUE if the map contains only valid characters. If not, will also
** display an error message
*/

static t_bool	validate_characters(t_cub_specs *cub_specs)
{
	int		i;
	int		j;
	int		line_len;
	t_bool	start_position;

	start_position = FALSE;
	i = 0;
	while (i < cub_specs->map_height)
	{
		j = 0;
		line_len = ft_strlen(cub_specs->map[i]);
		while (j < line_len)
		{
			if (!validate_character(&start_position, cub_specs->map[i][j]))
			{
				print_error("Invalid character in map");
				return (FALSE);
			}
			j++;
		}
		i++;
	}
	if (!start_position)
		print_error("Missing start position");
	return (start_position);
}

t_bool			validate_map(t_cub_specs *cub_specs)
{
	return (validate_characters(cub_specs) && check_map_closure(cub_specs));
}
