/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_resolution.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 18:05:53 by elopez-r          #+#    #+#             */
/*   Updated: 2020/07/31 20:39:26 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>

/*
** Returns TRUE if the resolution is positive. If it isn't, displays an
** error message
*/

t_bool	validate_resolution(t_cub_specs *cub_specs)
{
	if (cub_specs->x_resolution > 0 && cub_specs->y_resolution > 0)
		return (TRUE);
	else
	{
		print_error("Invalid resolution");
		return (FALSE);
	}
}
