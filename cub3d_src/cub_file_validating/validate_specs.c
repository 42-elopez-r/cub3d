/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_specs.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 20:13:31 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 18:06:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>

/*
** Performs all the validations in cub_specs returning TRUE if all of them
** pass
*/

t_bool	validate_cub_specs(t_cub_specs *cub_specs)
{
	return (validate_resolution(cub_specs) && validate_textures(cub_specs) &&
			validate_colors(cub_specs) && validate_sounds(cub_specs) &&
			validate_map(cub_specs));
}
