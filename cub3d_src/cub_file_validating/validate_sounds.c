/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_sounds.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 18:08:46 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 19:58:05 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>

/*
** Returns TRUE if all the sounds in cub_specs are readable files, displaying
** also an error if one isn't
*/

t_bool	validate_sounds(t_cub_specs *cub_specs)
{
	if (!is_file_readable(cub_specs->music))
		return (FALSE);
	else if (!is_file_readable(cub_specs->steps_sound))
		return (FALSE);
	else if (!is_file_readable(cub_specs->collision_sound))
		return (FALSE);
	else
		return (TRUE);
}
