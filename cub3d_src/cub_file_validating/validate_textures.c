/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_textures.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 17:43:26 by elopez-r          #+#    #+#             */
/*   Updated: 2020/10/24 18:10:05 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>

/*
** Returns TRUE if all the textures in cub_specs are readable files, displaying
** also an error if one isn't
*/

t_bool	validate_textures(t_cub_specs *cub_specs)
{
	if (!is_file_readable(cub_specs->north_texture))
		return (FALSE);
	else if (!is_file_readable(cub_specs->south_texture))
		return (FALSE);
	else if (!is_file_readable(cub_specs->west_texture))
		return (FALSE);
	else if (!is_file_readable(cub_specs->east_texture))
		return (FALSE);
	else if (!is_file_readable(cub_specs->sprite_texture))
		return (FALSE);
	else
		return (TRUE);
}
