/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_colors.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 18:12:12 by elopez-r          #+#    #+#             */
/*   Updated: 2020/07/31 20:39:15 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cub_file_validating.h>

/*
** Return TRUE if color is valid (fields between 0 and 255)
*/

static t_bool	validate_color(t_rgb *color)
{
	if (color->r < 0 || color->r > 255)
		return (FALSE);
	else if (color->g < 0 || color->g > 255)
		return (FALSE);
	else if (color->b < 0 || color->b > 255)
		return (FALSE);
	else
		return (TRUE);
}

/*
** Returns TRUE if all the colors in cub_specs are valid, printing an error
** message if one isn't
*/

t_bool			validate_colors(t_cub_specs *cub_specs)
{
	if (!validate_color(cub_specs->floor_color))
	{
		print_error("Invalid floor color");
		return (FALSE);
	}
	else if (!validate_color(cub_specs->ceiling_color))
	{
		print_error("Invalid ceiling color");
		return (FALSE);
	}
	else
		return (TRUE);
}
