/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 22:35:55 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/13 19:56:23 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdlib.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 32
# endif

size_t		ft_strlen(const char *s);
int			chr_pos(char *s, char c);
char		*copy_until_chr(char *s, char c);
void		trim_until_chr(char *s, char c);
int			get_next_line(int fd, char **line);

#endif
