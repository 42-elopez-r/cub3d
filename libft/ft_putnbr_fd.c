/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 16:10:59 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/03 18:40:46 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

void	display_number(long n, int fd)
{
	char c;

	if (n > 0)
	{
		c = n % 10 + '0';
		display_number(n / 10, fd);
		write(fd, &c, 1);
	}
}

void	ft_putnbr_fd(int n, int fd)
{
	long	nbr_l;

	nbr_l = n;
	if (nbr_l == 0)
	{
		write(fd, "0", 1);
		return ;
	}
	if (nbr_l < 0)
	{
		nbr_l *= -1;
		write(fd, "-", 1);
	}
	display_number(nbr_l, fd);
}
