/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 19:59:55 by elopez-r          #+#    #+#             */
/*   Updated: 2019/11/19 15:37:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void	del_nodes(t_list *node, void (*del)(void*))
{
	if (node->next)
		del_nodes(node->next, del);
	del(node->content);
	free(node);
}

void		ft_lstclear(t_list **lst, void (*del)(void*))
{
	del_nodes(*lst, del);
	*lst = NULL;
}
