/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 15:42:03 by elopez-r          #+#    #+#             */
/*   Updated: 2019/11/08 10:53:27 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	is_space(char c)
{
	return (c == ' ' || c == '\t' || c == '\n' || c == '\f' ||
			c == '\v' || c == '\r');
}

int			ft_atoi(const char *nptr)
{
	long	acm;
	int		sign;

	while (*nptr && is_space(*nptr))
		nptr++;
	if (*nptr == '+' || *nptr == '-')
	{
		sign = *nptr == '+' ? 1 : -1;
		nptr++;
	}
	else
		sign = 1;
	acm = 0;
	while (*nptr >= '0' && *nptr <= '9')
	{
		acm = acm * 10 + (*nptr - '0');
		nptr++;
	}
	acm *= sign;
	return ((int)acm);
}
