/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/11 17:55:30 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/03 17:45:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	find_start(char const *s1, char const *set)
{
	size_t i;

	i = 0;
	while (s1[i])
	{
		if (!ft_strchr(set, s1[i]))
			return (i);
		i++;
	}
	return (i);
}

static size_t	find_end(char const *s1, char const *set)
{
	size_t i;
	size_t end;

	if (ft_strlen(s1) == 0)
		return (0);
	i = ft_strlen(s1) - 1;
	end = i + 1;
	while (i > 0)
	{
		if (!ft_strchr(set, s1[i]))
			return (i);
		i--;
	}
	if (!ft_strchr(set, s1[i]))
		return (i);
	return (end);
}

char			*ft_strtrim(char const *s1, char const *set)
{
	char	*trimmed;
	size_t	start;
	size_t	end;
	size_t	i;
	size_t	j;

	start = find_start(s1, set);
	end = find_end(s1, set);
	if ((trimmed = malloc(ft_strlen(s1) - start - (ft_strlen(s1) - end) + 2)))
	{
		i = 0;
		j = start;
		while (i <= end - start)
			trimmed[i++] = s1[j++];
		trimmed[i] = '\0';
		return (trimmed);
	}
	else
		return (NULL);
}
