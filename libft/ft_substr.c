/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 11:51:19 by elopez-r          #+#    #+#             */
/*   Updated: 2019/11/30 17:08:08 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	substr_len(char const *s, unsigned int start, size_t max_len)
{
	int		i;
	size_t	len;

	if (start >= ft_strlen(s))
		return (0);
	i = start;
	len = 0;
	while (s[i] && len < max_len)
	{
		i++;
		len++;
	}
	return (len);
}

char		*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*substr;
	int		i;
	size_t	j;

	if ((substr = malloc(substr_len(s, start, len) + 1)))
	{
		i = start;
		j = 0;
		if (start < ft_strlen(s))
		{
			while (s[i] && j < len)
				substr[j++] = s[i++];
		}
		substr[j] = '\0';
		return (substr);
	}
	else
		return (NULL);
}
