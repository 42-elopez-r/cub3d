# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile-so-minilibx                               :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/07/29 00:59:08 by elopez-r          #+#    #+#              #
#    Updated: 2020/12/14 14:44:44 by elopez-r         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = cub3D

CC = cc
MAKE = make
SHELL = /bin/sh

CFLAGS += -Wall -Wextra -Werror -I include -D PROG_NAME=\"$(NAME)\"
LDFLAGS += -lm -lmlx -lXext -lX11

LIBFT_A = libft/libft.a
GET_NEXT_LINE_A = get_next_line/get_next_line.a

SRC = $(shell find cub3d_src/ -name "*.c")

OBJS = $(SRC:%.c=%.o)

all: $(NAME)

$(NAME): libft get_next_line $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(GET_NEXT_LINE_A) $(LIBFT_A) $(LDFLAGS)

libft:
	$(MAKE) -C libft bonus

get_next_line:
	$(MAKE) -C get_next_line

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

bonus: $(NAME)

clean:
	$(MAKE) -C libft clean
	$(MAKE) -C get_next_line clean
	rm -f $(OBJS)

fclean: clean
	$(MAKE) -C libft fclean
	$(MAKE) -C get_next_line fclean
	rm -f $(NAME)

re: fclean all

debug: CFLAGS += -g
debug: fclean all

.PHONY: all clean fclean re bonus libft get_next_line
